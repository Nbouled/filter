<?php

namespace App\Filters;

// this Trait can be used for any other Model to filter
trait Filterable
{
    public function scopeFilter($query, QueryFilters $filters)
    {
        return $filters->apply($query);
    }

}
