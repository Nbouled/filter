<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // add some fake data to play with :)
        Company::factory(3)->create()->each(function (Company $c) {
            User::factory(200)->create(['company_id' => $c->id]);
        });
    }
}
