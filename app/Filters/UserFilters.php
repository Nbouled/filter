<?php

namespace App\Filters;

class UserFilters extends QueryFilters
{
    // some filter examples..

    public function name($value)
    {
        return $this->builder->where(['name' => $value]);
    }

    public function country($value)
    {
        return $this->builder->where(['country' => $value]);
    }

    public function age($value)
    {
        return $this->builder->where(['age' => $value]);
    }

    public function isIntern($value)
    {
        return $this->builder->where(['is_intern' => $value]);
    }

    public function gender($value)
    {
        return $this->builder->where(['gender' => $value]);
    }

    // some filter by relationship

    public function companyName($value)
    {
        return $this->builder->whereHas('company', function ($query) use ($value) {
            return $query->where(['name' => $value]);
        });
    }

    public function companyCity($value)
    {
        return $this->builder->whereHas('company', function ($query) use ($value) {
            return $query->where(['city' => $value]);
        });
    }


    // some sort functions
    public function sortGender()
    {
        return $this->builder->orderBy('gender', 'desc');
    }

    public function sortAge()
    {
        return $this->builder->orderBy('age', 'desc');
    }

}
