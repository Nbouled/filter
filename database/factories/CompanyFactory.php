<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'country' => $this->faker->country(),
            'city' => $this->faker->city(),
            'value' => $this->faker->numberBetween(100000,750000),
        ];
    }
}
