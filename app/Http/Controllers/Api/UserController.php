<?php

namespace App\Http\Controllers\Api;

use App\Filters\UserFilters;
use App\Http\Controllers\Controller;
use App\Models\User;


class UserController extends Controller
{

    // dependency injection
    public function index(UserFilters $userFilters)
    {
        // get relationship by Eager loading, this will add the company in to the collection.
        $users = User::with('company')->filter($userFilters)->get();

        return response()->json([
            'users' => $users
        ]);
    }

    //In this way the controller and models can stay slim even do the user might have 50+ parameters to filter.
}
